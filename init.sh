#!/bin/bash

sudo rm -rf ~/dotfiles/.vim/*

curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
# For example, we just use `~/.cache/dein` as installation directory
sh ./installer.sh ~/dotfiles/.vim/bundles

sudo chmod -R a+rwx .vim/
