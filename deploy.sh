#!/bin/bash

sudo rm -rf ~/.vim
rm ~/.vimrc
rm ~/.bashrc
rm ~/.profile
rm ~/.tmux.conf

ln -s ~/dotfiles/.vim ~/.vim
ln -s ~/dotfiles/.vimrc ~/.vimrc
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.profile ~/.profile
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
