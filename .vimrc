" derived from sample
set backspace=indent,eol,start

set nobackup		" do not keep a backup file, use versions instead
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" setting
"文字コードをUFT-8に設定
set fenc=utf-8
" スワップファイルを作らない
set noswapfile " 編集中のファイルが変更されたら自動で読み直す
set autoread
" バッファが編集中でもその他のファイルを開けるように
set hidden
" 入力中のコマンドをステータスに表示する
set showcmd



" 見た目系
" 行番号を表示
set relativenumber number
" 現在の行を強調表示
set cursorline
" 現在の行を強調表示（縦）
" set cursorcolumn
" 行末の1文字先までカーソルを移動できるように
set virtualedit=onemore
" インデントはスマートインデント
set smartindent
" ビープ音を可視化
"set visualbell
" 括弧入力時の対応する括弧を表示
set showmatch
" ステータスラインを常に表示 set laststatus=2
" コマンドラインの補完
set wildmode=list:longest
" 折り返し時に表示行単位での移動できるようにする
nnoremap j gj
nnoremap k gk
" シンタックスハイライトの有効化
syntax enable

" カーソルの形をモードに応じて変更
if has('vim_starting')
  " 挿入モード時に非点滅の縦棒タイプのカーソル
  let &t_SI .= "\e[6 q"
  " ノーマルモード時に非点滅のブロックタイプのカーソル
  let &t_EI .= "\e[2 q"
  " 置換モード時に非点滅の下線タイプのカーソル
  let &t_SR .= "\e[4 q"
endif


" Tab系
" 不可視文字を可視化(タブが「▸-」と表示される)
" set list listchars=tab:\▸\-
" Tab文字を半角スペースにする
set expandtab
" 行頭以外のTab文字の表示幅（スペースいくつ分）
set tabstop=2
" 行頭でのTab文字の表示幅
set shiftwidth=2


" 検索系
" 検索文字列が小文字の場合は大文字小文字を区別なく検索する
set ignorecase
" 検索文字列に大文字が含まれている場合は区別して検索する
set smartcase
" 検索文字列入力時に順次対象文字列にヒットさせる
set incsearch
" 検索時に最後まで行ったら最初に戻る
set wrapscan
" 検索語をハイライト表示
set hlsearch
" ESC連打でハイライト解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>


" sync with OS clip board
set clipboard=unnamed,unnamedplus

" mapping系
inoremap <silent> jj <ESC>
"unable C-Z and replace as undo
inoremap <C-Z> <Esc>ui		
nnoremap <C-Z> u

"unable C-S and replace as save
inoremap <C-s> <Esc>:w<CR>
nnoremap <C-s> :w<CR>

""use arrow key as gj, gk
"nnoremap <Down> gj
"nnoremap <Up> gk

"enable commands at IME
nnoremap あ a
nnoremap い i
nnoremap う u
nnoremap お o
nnoremap ｄｄ dd
nnoremap ｙｙ yy
nnoremap っd dd


"dein Scripts-----------------------------


if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.vim/bundles/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('~/.vim/bundles')
  call dein#begin('~/.vim/bundles')

  " Let dein manage dein
  " Required:
  call dein#add('~/.vim/bundles/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here like this:
  call dein#add('Shougo/neocomplcache.vim')
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')

  " ファイルをtree表示してくれる
  call dein#add("scrooloose/nerdtree")
  call dein#add("jistr/vim-nerdtree-tabs")
  "コメントアウト 
  call dein#add("tpope/vim-commentary")
  " git
  call dein#add("tpope/vim-fugitive")
  " show gitdiff always
  call dein#add("airblade/vim-gitgutter")
  " color scheme
  call dein#add("jacoborus/tender.vim")
  " call dein#add("raphamorim/lucario")
  " call dein#add("gosukiwi/vim-atom-dark")
  call dein#add("jdkanani/vim-material-theme")
  " call dein#add("sjl/badwolf")
  " call dein#add("tomasr/molokai")
  call dein#add("tomasiser/vim-code-dark")
  " status line
  call dein#add("itchyny/lightline.vim")
  call dein#add("vim-airline/vim-airline")
  call dein#add("vim-airline/vim-airline-themes")

  " mark down preview
  call dein#add('tpope/vim-markdown')
  call dein#add('kannokanno/previm')
  call dein#add('tyru/open-browser.vim')

  " edit surround text
  call dein#add('tpope/vim-surround')
  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif
call map(dein#check_clean(), "delete(v:val, 'rf')")
"End dein Scripts-------------------------

"airline---------------------------------


" Theme---------------------------------
colorscheme tender
" colorscheme badwolf
let g:airline_theme = 'codedark'
highlight Normal ctermbg=none


" neo complcache
let g:neocomplcache_enable_at_startup = 1 " 起動時に有効化
inoremap <expr><CR>  neocomplcache#smart_close_popup() . "\<CR>"
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" 補完表示時のEnterで改行をしない
inoremap <expr><CR>  pumvisible() ? "<C-y>" : "<CR>"

" 補完プレビューを表示しない
set completeopt=menuone

"NERDTree-------------------------------
"let g:NERDTreeShowBookmarks=1

"autocmd vimenter * NERDTree

"引数なしでvimを開いたらNERDTreeを起動、
"引数ありならNERDTreeは起動せず、引数で渡されたファイルを開く。
autocmd vimenter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" autocmd vimenter * if !argc() | NERDTree | endif

" map <C-n> :NERDTreeToggle<CR>

"他のバッファをすべて閉じた時にNERDTreeが開いていたらNERDTreeも一緒に閉じる。
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" 隠しファイルを表示する
let NERDTreeShowHidden = 1

nnoremap <C-i> <C-w>w<CR>
map <C-n> <plug>NERDTreeTabsToggle<CR>
let g:nerdtree_tabs_open_on_console_startup=1
map <C-l> gt
map <C-h> gT



" markdown 
   autocmd BufRead,BufNewFile *.mkd  set filetype=markdown
   autocmd BufRead,BufNewFile *.md  set filetype=markdown
   " Need: kannokanno/previm
   nnoremap <silent> <C-p> :PrevimOpen<CR> " Ctrl-pでプレビュー
   " 自動で折りたたまないようにする
   let g:vim_markdown_folding_disabled=1
   let g:previm_enable_realtime = 1


"vim-terminal---------------------------


" set splitbelow
" set termwinsize=10x0
" function! TermOpen()
"   if empty(term_list())
"     execute "terminal"
"   endif
" endfunction
" noremap <silent> <space>t :call TermOpen()<CR>
