## setup

if needed 
```
sudo apt install curl
mkdir ~/dotfiles/.vim
```
```
chmod +x dotfiles/setup.sh 
cd ~/dotfiles
./setup.sh
```

## caution  
To use dein,
vim version must be larger than 8.0  
Please install latest vim
```
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt install vim
```

To use clipboard
```
sudo apt install xsel
```
Finally if you want to use tmux, please install.
```
sudo apt install tmux
```
